<?php
/**
 * The template for displaying all single posts.
 */

get_header();

global $post;

if ( wp_attachment_is_image( $post->id ) ) {
    $image = wp_get_attachment_image_src( $post->id, 'full' );
    $image_url = ( $image ) ? $image[0] : '';
}

?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        
        <section id="banner">
            <div class="container">
                <div class="row">
                    <div class="blog-header text-center">
                        <h2>Din ogradă</h2>
                        <ul class="breadcrumb">
                            <li><?php the_title(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-single">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <h4><?php the_title(); ?></h4>
                        <ul class="post-meta-links list-inline">
                            <li><a href="#"><span> <i class="fa fa-bookmark"></i></span> <?php echo strtolower( get_the_time( 'j F Y' ) ); ?></a></li>
                        </ul>
                        <div class="post-gallery">
                            <div class="gallery-nav-link gallery-nav-left"><?php previous_image_link( false, __( 'Previous', 'plai' ) ); ?></div>
                            <figure class="gallery-image">
                                <img src="<?php echo $image_url; ?>" alt="<?php the_title(); ?>" />
                            </figure>
                            <div class="gallery-nav-link gallery-nav-right"><?php next_image_link( false, __( 'Next', 'plai' ) ); ?></div>
                        </div>
                    </div>
                    <hr />

                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();