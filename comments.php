<?php if ( have_comments() ) : ?>
    <div class="comments">
        <h4>Păreri</h4>
        <hr />
            <ul class="media-list">
<?php
                wp_list_comments( array(
                    'style'      => 'ul',
                    'callback'   => 'plai_the_comment',
                    'short_ping' => true,
                    'echo' => true,
                    'per_page' => -1
                ) );
?>
            </ul>
    </div>
<?php endif; ?>     

<div class="blog-form">
    <h4>Spune-ne ce crezi.</h4>
    <hr />
<?php
    $args = array(
        'comment_field'        => '<div class="form-group"><div class="col-md-12"><textarea id="comment" name="comment" cols="30" rows="7" class="form-control" placeholder="Mesajul tău"  aria-required="true" required="required"></textarea></div></div>',
        'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'plai' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( get_the_ID() ) ) ) ) . '</p>',
        'logged_in_as'         => '',
        'comment_notes_before' => '',
        'comment_notes_after'  => '',
        'fields'               => '<div class="form-group"><div class="col-md-6"><input id="author" name="author" type="text" class="form-control" placeholder="Name" aria-required="true" required="required" /></div></div><div class="form-group"><div class="col-md-6"><input id="email" name="email" type="email" class="form-control" placeholder="Email" aria-describedby="email-notes" aria-required="true" required="required" /></div></div>',
        'id_form'              => 'commentform',
        'id_submit'            => 'submit',
        'class_submit'         => 'submit',
        'name_submit'          => 'submit',
        'title_reply'          => '',
        'title_reply_to'       => '',
        'cancel_reply_link'    => ' ',
        'submit_button'        => '<div class="col-md-12"><div class="text-center"><button id="%2$s" class="%3$s btn btn-main" name="%1$s" value="%4$s">Trimite</button></div></div>',
        'submit_field'         => '<p class="form-submit">%1$s %2$s</p>',
        'format'               => 'xhtml'
    );

    // Call the WordPress native comment form function.
    comment_form( $args );
?>
</div>