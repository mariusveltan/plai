<?php
/**
 * The template for displaying the footer.
 */

?>

<!-- Footer Area Start -->

<section id="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class=" col-md-3 col-sm-4 col-xs-6">
                    <h3 class="menu_head">Contact</h3>
                    <div class="footer_menu_contact">
                        <ul>
                            <li><i class="fa fa-phone"></i>
                                <span><a href="mailto:birou@plai.ro" title="birou@plai.ro">birou@plai.ro</a></span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="footer_menu">
<?php
                        wp_nav_menu(
                            array(
                                'container' => false,
                                'theme_location' => 'footer-first-column'
                            )
                        );
?>
                    </div>

                </div>
                <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="footer_menu">
<?php
                        wp_nav_menu(
                            array(
                                'container' => false,
                                'theme_location' => 'footer-second-column'
                            )
                        );
?>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="footer_b">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <div class="footer_bottom">
                        <p class="text-block"> &copy; Copyright reserved to <span>Plai festival </span></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="footer_mid pull-right">
                        <ul class="social-contact list-inline">
                            <li> <a href="https://www.facebook.com/PLAIFestival"><i class="fa fa-facebook"></i></a></li>
                            <li> <a href="https://twitter.com/PLAIFestival"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="https://plus.google.com/u/0/+PlaiRo/posts"><i class="fa fa-google-plus"></i> </a></li>
      
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Footer Area End -->

<!-- Back To Top Button -->
    <div id="back-top">
        <a href="#slider_part" class="scroll" data-scroll>
            <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-up"></i></button>
        </a>
    </div>
    <!-- End Back To Top Button -->

<?php wp_footer(); ?>

</body>
</html>