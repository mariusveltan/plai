<?php
/**
 * The template for rendering the site landing page.
 */

get_header(); ?>
 <!-- Slider start -->
    <section id="slider_part">
         <div class="carousel slide" id="carousel-example-generic" data-ride="carousel">
            <!-- Indicators -->
             <ol class="carousel-indicators text-center">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
             </ol>

            <div class="carousel-inner">
                <div class="item active">
                    <div class="overlay-slide">
                        <img src="<?php plai_the_theme_root_uri( 'static/img/banner/banner.jpg' ); ?>" alt="" class="img-responsive">
                    </div>
                    <div class="carousel-caption">
                        <div class="col-md-12 col-xs-12 text-center">
                      <h3 class="animated2">8 - 11 septembrie 2016 </h3> 
                      <div class="line" style="color:green;"></div>
                      <p class="animated3">Muzeul Satului Bănăţean Timişoara</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="overlay-slide">
                        <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>banner/1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="carousel-caption">
                    <div class="col-md-12 col-xs-12 text-center">
                      <h3>Abonament 4 zile </h3> 
                      <div class="line" style="color:green;"></div>
                      <p >8 - 11 septembrie: 110 lei</p>
                      <a style="none" href ="http://www.eventim.ro/ro/bilete/plai-2016-editia-a-ii-a-timisoara-muzeul-satului-banatean-419966/event.html" class="btn-success btn-lg " role="button">De aici</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="overlay-slide">
                        <img src="<?php plai_the_theme_root_uri( 'static/img/banner/plai.jpg' ); ?>" alt="" class="img-responsive">
                    </div>
                    <div class="carousel-caption">
                    <div class="col-md-12 col-xs-12 text-center">
                       <h3>Bilet 1 zi </h3>
                      <div class="line" style="color:green;"></div>
                      <p>9, 10, 11 septembrie: 50 lei</p>
                      <a style="none" href ="http://www.eventim.ro/ro/bilete/plai-2016-editia-a-ii-a-timisoara-muzeul-satului-banatean-419966/event.html" class="btn-success btn-lg " role="button">De aici</a>
                        </div>
                    </div>
                </div>

             </div>      <!-- End Carousel Inner -->

            <!-- Controls -->
            <div class="slides-control ">
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span><i class="fa fa-angle-left"></i></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span><i class="fa fa-angle-right"></i></span>
                </a>
            </div>
        </div>
    </section>
    <!--/ Slider end -->

<div class="clearfix"></div>

<!-- About us start -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Ce trebuie să ştii</h3>
                    <h4 class="feature_sub"> </h4>
                    <div class="divider"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="feature-tab">
               <div class="col-md-2 col-sm-3 col-xs-12">
                    <ul class="nav-tabs main-tab-list text-center" role="tablist">
                          <li role="presentation" class="active">
                            <a href="#home" role="tab" data-toggle="tab" >
                              <div class="single-tab">
                                    <div class="f-icon">
                                        <i class="fa">
                                         <img src="<?php plai_the_theme_root_uri( 'static/img/icons/plai-e-personal.png' ); ?>" alt="">
                                        </i>
                                    </div>
                                </div>
                                 <h4>Plai e personal</h4>
                            </a>
                          </li>
                          <li role="presentation" >
                            <a href="#profile" role="tab" data-toggle="tab">
                                <div class="single-tab">
                                    <div class="f-icon">
                                        <i class="fa">
                                         <img src="<?php plai_the_theme_root_uri( 'static/img/icons/artisti-si-parteneri.png' ); ?>" alt="">
                                        </i>
                                    </div>
                                </div>
                                <h4>Artişti şi parteneri culturali</h4>
                            </a>
                          </li>
                          <li role="presentation" >
                            <a href="#messages" role="tab" data-toggle="tab">
                                <div class="single-tab">
                                    <div class="f-icon">
                                        <i class="fa">
                                         <img src="<?php plai_the_theme_root_uri( 'static/img/icons/bilete.png' ); ?>" alt="">
                                        </i>
                                    </div>
                                </div>
                                <h4>Bilete</h4>
                            </a>
                          </li>
                        <!--  <li role="presentation" >
                            <a href="#photo" role="tab" data-toggle="tab">
                                <div class="single-tab">
                                    <div class="f-icon">
                                        <i class="fa">
                                          <img src="<?php plai_the_theme_root_uri( 'static/img/icons/cum-ajut.png' ); ?>" alt="">
                                        </i>
                                    </div>
                                </div>
                                <h4>2%</h4>
                            </a>
                          </li> -->
                            <li role="presentation" >
                            <a href="#ios" role="tab" data-toggle="tab">
                              <div class="single-tab">
                                    <div class="f-icon">
                                        <i class="fa">
                                         <img src="<?php plai_the_theme_root_uri( 'static/img/icons/regulament.png' ); ?>" alt="">
                                        </i>
                                    </div>
                                </div>
                               <h4>Info</h4>
                            </a>
                          </li>                      
                    </ul>
                </div>  <!-- col-md-12 end -->
                <div class="col-md-10 col-sm-9 col-xs-12">
                    <div style=" image-width=50%"class="tab-content main-tab-content">
                          <div role="tabpanel" class="tab-pane active " id="home">
                                <div class="col-md-12 col-sm-9">
                                     <img src="<?php plai_the_theme_root_uri( 'static/img/about/plai-e-personal2.jpg' ); ?>" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-12 col-sm-9">
                                    <div class="c-tab">
                                         <h4>Arată-ne viziunea ta personala asupra festivalului</h4>
                                         <p>De zece ani trăim în fiecare septembrie experiențe care ne aduc mai aproape unii de alții și ne apropie prin dragostea de artă, muzică și cultură. Festivalul PLAI trăiește în continuare prin voi, prieteni, sponsori şi finanţatori, public, parteneri și artiști. Ne bucurăm mereu de atmosfera pe care o simțim atunci când ne vedem printre căsuțe, printre raze de soare sau când ne găsim ascunși printre culori și lampioane. </p>
                                         <p>
                                             Vrem să vedem cum definești tu festivalul. Creează o variantă personală a afișului de anul acesta și ai șansa să câștigi invitații la PLAI. Descarcă imaginea cu conturul afișului și joacă-te, coloreaz-o prin orice tehnică vrei tu, apoi încarcă rezultatul pe Facebook și dă tag paginii PLAI. Autorii afișelor cu cel mai mare număr de Like-uri (top 3) pe Facebook vor primi din partea noastră câte un abonament la festival. Ai timp până în 13 mai să ne arăți viziunea ta personală asupra festivalului.
                                         </p>
                                         <br>
                                         <a href="#"> Descarca afisul de aici</a>
                                    </div>
                                </div>
                                
                          </div>
                          <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="col-md-12 col-sm-9">
                                     <img src="<?php plai_the_theme_root_uri( 'static/img/about/artisti-si-parteneri.jpg' ); ?>" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-12 col-sm-9">
                                    <div class="c-tab">
                                         <h4>Pe cine vei intalni anul acesta la Plai</h4>
                                         <p>Detalii în curând</p>
                                         <br>
                                         <a href="plaiXI.html"> Vezi toti artistii si partenerii culturali din acest an</a>
                                    </div>
                                </div>
                          </div>
                          <div role="tabpanel" class="tab-pane" id="messages">
                               <div class="col-md-12 col-sm-9">
                                     <img src="<?php plai_the_theme_root_uri( 'static/img/about/bilete.jpg' ); ?>" alt="" class="img-responsive">
                                </div>
                               <div class="col-md-12 col-sm-9">
                                    <div class="c-tab">
                                         <h4>Bilete</h4>
                                         <p>Prețul de pre-sale al unui abonament (3 zile) la ediția a XI-a a festivalului PLAI este de 75 lei, în perioada 10 Martie – 6 Iunie și asigură accesul la toate evenimentele dedicate publicului participant.</p>
                                         <p>Persoanele cu dizabilități au intrarea gratuită, împreună cu un însoțitor.</p>
                                         <br>
                                         <p>Biletele sunt disponibile în reţeaua Eventim (online sau la magazinele Orange, Vodafone, Germanos ,reteaua librăriilor Cărtureşti şi la Ambasada).
                                         </p>
                                         <br>
                                        <a href="#"> Cumpara un bilet </a>
                                    </div>
                                </div>
                          </div>
                        <!--  <div role="tabpanel" class="tab-pane" id="photo">
                               <div class="col-md-12 col-sm-9">
                                     <img src="<?php plai_the_theme_root_uri( 'static/img/about/voluntari.jpg' ); ?>" alt="" class="img-responsive">
                                </div>
                               <div class="col-md-12 col-sm-9">
                                    <div class="c-tab">
                                        <h4>Sustine Plai</h4>
                                         <p>Vă place Plai? Atunci puteţi să contribuiţi şi voi la creşterea festivalului în viitor. Nu este un secret pentru nimeni că organizarea unei astfel de manifestări presupune, pe lângă foarte multă muncă, şi nişte sume considerabile. Noi, Asociaţia Culturală Sunet Ambianţă Timişoara (A.C.S.A.T), suntem o organizaţie tânără, non-guvernamentală, non-profit, care îşi propune promovarea artei şi a multiculturalităţii, dorind să creeze şi să ofere ocazia de dezvoltare prin diversitate. Practic, fiecare leu care intră în conturile organizaţiei este folosit pentru următoarea ediţie a Festivalului PLAI. Cu cât fondurile disponibile sunt mai mari, la Plai vor veni artişti mai mari şi mai mulţi.</p>
                                         <br>
                                        <a href="#"> Afla mai multe</a>
                                    </div>
                                </div>
                          </div> -->
                            <div role="tabpanel" class="tab-pane" id="ios">
                                <div class="col-md-12 col-sm-9">
                                     <img src="<?php plai_the_theme_root_uri( 'static/img/about/info.jpg' ); ?>" alt="" class="img-responsive">
                                </div>
                               <div class="col-md-12 col-sm-9">
                                    <div class="c-tab">
                                         <h4>Informatii utile</h4>
                                         <p>Vineri deschidem porțile muzeului de la ora 17:00 iar sâmbătă și duminică de la ora 12:00.</p>
                                         <br><a href="transport.html"> Cum ajung?</a><br>
                                          <a href="camping.html"> Camping</a><br>
                                         <a href="regulament.html"> Regulament</a>
                                    </div>
                                </div>
                             </div>
                    </div>
                </div>

                 
            </div>
        </div>
    </div>
</section>
<!-- About us End -->

<!-- Team MEmber Start -->

<section id="about-details">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Din ogradă</h3>
                    <h4 class="feature_sub"><b> Blog<b></h4>
                    <div class="divider"></div>
                </div>
            </div>  <!-- Col-md-12 End -->

            <div id="din-ograda" class="owl-carousel owl-theme team-items">
<?php
              $args = array(
                          'category_name' => 'din-ograda',
                          'posts_per_page' => 8,
                          
                      );
              $blog_posts = new WP_Query( $args );
              while ( $blog_posts->have_posts() ) {
                $blog_posts->the_post();
                get_template_part( 'template-parts/content', 'preview-din-ograda' );
              }
?>
            </div>
        </div>
    </div> <!-- Conatiner Team end -->
    <!-- </div> -->
</section>  
<div class="clearfix"></div>



<div class="clearfix"></div>
<section id="video-fact">
    <div class="container">
         <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Ce e PLAI</h3>
                    <h4 class="feature_sub"><b>Povestea noastră <b></h4>
                    <div class="divider"></div>
                </div><br><br> 
            </div> <!-- Col-md-12 End -->
                 <div class="col-md-6 ">
                    <div class="landing-video">
                        <div class="video-embed wow fadeIn" data-wow-duration="1s">
                                <!-- Change the url -->
                            <iframe src="https://www.youtube.com/embed/uwysLh9FeM4" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="video-text">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading active" role="tab" id="headingOne">
                              <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                               PLAI s-a născut într-o zi de vară
                                </a>
                              </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body p1">
                               de iunie, după o vizită la Muzeul Satului Bănăţean. Acest loc este, în continuare, de mare importanţă pentru festival, fiind un eveniment care să îi celebreze diversitatea şi multiculturalitatea, unul care să readucă în conştiinţa colectivă ceea ce ne-a fost lăsat ca moştenire, ceea ce ne face să fim români, bănăţeni, timişoreni.
                              </div>
                            </div>
                          </div>
                      <div class="panel panel-default">
                        <div class="panel-heading " role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="accordion-toggle collapsed"  data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                             PLAI nu este doar un festival de muzică. 
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body p1">
                            Este un eveniment pentru toate vârstele, plin de activităţi menite să nască pasiuni şi presărat cu muzică bună din toate colţurile lumii. Diversitatea, în toate formele ei, este la ea acasă. Astfel s-a născut acest eveniment-fenomen care, pe lângă world music, promovează şi dezvoltă o reţea importantă de organizaţii non-profit şi mediu creativ, cu peste 50 de entităţi implicate în fiecare an.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading " role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Prima ediţie a festivalului s-a creionat în doar două luni
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body p1">
                             iar echipa a crescut organic dintr-un general “eu cu ce vă pot ajuta”. Am fost în asentimentul multor timişoreni care doreau altceva de la un festival. Aşa ne-am format echipa. Suntem, cu toţii, voluntari (PLAI va rămâne mereu 100% voluntariat), venind din medii cât mai diverse, cu interese diferite, dar cu dragoste pentru PLAI. Am ajuns să numărăm a 10-a ediţie (în 2015) datorită echipei de voluntari, dar şi datorită finanţatorilor, partenerilor culturali, a publicului PLAI. Festivalul este al lor, în egala măsură, şi va exista câtă vreme această muncă colectiva va continua.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading " role="tab" id="headingFour">
                          <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                              Fiecare an este o aniversare
                            </a>
                          </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                          <div class="panel-body p1">
                             şi fiecare ediţie este, în multe privinţe, cum a fost prima. Un an întreg visăm la fiecare PLAI şi suntem recunoscători pentru fiecare dintre activităţi, aşa diverse şi unice cum sunt, nu numai datorită echipei de voluntari, dar şi a finanţatorilor, sponsorilor, partenerilor culturali şi, foarte important pentru noi, a tuturor celor care vin să se bucure de el, cumpără bilet, finanţând astfel evenimentul.
                          </div>
                        </div>
                      </div>                     
                    </div>
                </div>
            </div> 
        </div><!-- row End -->
    </div>
</section>


<!-- Client Carousel Start -->
  <section id="client">
    <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Ediţii anterioare</h3>
                </div>
            </div>  <!-- Col-md-12 End -->
        <div class="row wow fadeInLeft">
          <div id="client-carousel" class="col-sm-12 owl-carousel owl-theme text-center client-carousel">
            <div class="item client_logo">
              <a href="#">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2015.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2015</h3>
            </div>
            <div  class="item client_logo">
              <a href="https://issuu.com/plai-festival/docs/2014_caiet_program_plai" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2014.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2014</h3>
              
            </div>
            <div class="item client_logo">
              <a href="https://issuu.com/plai-festival/docs/2013_caiet_program_plai__1_" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2013.jpg" alt="client" class="img-responsive">
                </a>
                <h3 class="text-center">2013</h3>
            </div>
            <div class="item client_logo">
              <a href="https://issuu.com/plai-festival/docs/2012_caiet_program_plai" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2012.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2012</h3>
              
            </div>
            <div class="item client_logo">
              <a href="https://www.facebook.com/PLAIFestival/photos/?tab=album&album_id=10150319061414877" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2011.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2011</h3>
            
            </div>
            <div class="item client_logo">
              <a href="#">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2010.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2010</h3>
            
            </div>
            <div class="item client_logo">
              <a href="https://www.facebook.com/PLAIFestival/photos/?tab=album&album_id=155528029876" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2009.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2009</h3>
            
            </div>
            <div class="item client_logo">
              <a href="https://www.facebook.com/PLAIFestival/photos/?tab=album&album_id=85926664876" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2008.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2008</h3>
            
            </div>
                        <div class="item client_logo">
              <a href="https://www.facebook.com/PLAIFestival/photos/?tab=album&album_id=85934884876" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2007.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2007</h3>
            
            </div>
                        <div class="item client_logo">
              <a href="https://www.facebook.com/PLAIFestival/photos/?tab=album&album_id=10151408624564877" target="_blank">
                <img src="<?php plai_the_theme_root_uri( 'static/img/' ); ?>afise/2006.jpg" alt="client" class="img-responsive">
              </a><h3 class="text-center">2006</h3>
            
            </div>
          </div><!-- Owl carousel end -->
        </div><!-- Main row end -->
    </div>
  </section>
<!-- Client Carousel End -->
 
<?php get_footer(); ?>