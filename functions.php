<?php
/**
 * Functions and definitions for theme setup.
 */

/*
 * Establish the theme version.
 */
global $plai_version;
$plai_template = wp_get_theme()->get( 'Template' );
$plai_version  = wp_get_theme( $plai_template )->get( 'Version' );



// Set the theme root path and URL.
define( 'PLAI_ROOT_PATH', get_template_directory() );
define( 'PLAI_ROOT_URI', get_template_directory_uri() );


// Require theme additional functionality.
require PLAI_ROOT_PATH . '/inc/helpers.php';
require PLAI_ROOT_PATH . '/inc/post-types.class.php';
require PLAI_ROOT_PATH . '/inc/template-tags.php';
require PLAI_ROOT_PATH . '/inc/widgets/gallery.class.php';



if ( ! function_exists( 'plai_setup' ) ) :
    /**
     * Set up theme defaults and register support for WordPress features.
     */
    function plai_setup() {
        // Make theme available for translation.
        load_theme_textdomain( 'plai', PLAI_ROOT_PATH . '/languages' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        // Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );
        // Declare the thumbnail sizes.
        set_post_thumbnail_size( 270, 240, true );
        add_image_size( 'plai-225-140', 225, 140, true );
        add_image_size( 'plai-500-403', 500, 403, true );

        // Register theme nav menus.
        register_nav_menus( array(
            'primary' => esc_html__( 'Primary', 'plai' ),
            'footer-first-column' => esc_html__( 'Footer First Column', 'plai' ),
            'footer-second-column' => esc_html__( 'Footer Second Column', 'plai' ),
        ) );

        /*
         * Switch default core markup for search form,
         * comment form, and comments to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
    }
endif;

add_action( 'after_setup_theme', 'plai_setup' );


if ( ! function_exists( 'plai_enqueue_scripts' ) ) :
    /**
     * Enqueue scripts and styles.
     */
    function plai_enqueue_scripts() {
        global $plai_version;

        wp_register_style( 'plai-fonts', PLAI_ROOT_URI . '/static/fonts/MyFontsWebfontsKit/fonts.css', array(), $plai_version );
        wp_register_style( 'plai-style', PLAI_ROOT_URI . '/style.css', array( 'plai-fonts' ), $plai_version );
        wp_enqueue_style( 'plai-fonts');
        wp_enqueue_style( 'plai-style');

        wp_register_script( 'plai-main', get_template_directory_uri() . '/static/js/main.min.js', array(), $plai_version, true );
        wp_enqueue_script( 'plai-main' );

        if ( is_singular() &&
                comments_open() &&
                get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }
endif;

add_action( 'wp_enqueue_scripts', 'plai_enqueue_scripts' );



if ( ! function_exists( 'plai_widgets_init' ) ):

    /**
     * Register sidebars and widgets.
     */
    function plai_widgets_init() {
        // Register sidebars support.
        register_sidebar( array(
            'name'          => esc_html__( 'Article Sidebar', 'plai' ),
            'id'            => 'article-sidebar',
            'description'   => esc_html__( 'The right sidebar section of the blog article template.', 'plai' ),
            'before_widget' => '<div class="sidebar-section">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        ) );
        register_sidebar( array(
            'name'          => esc_html__( 'Participant Sidebar', 'plai' ),
            'id'            => 'participant-sidebar',
            'description'   => esc_html__( 'The right sidebar section of the participant page template.', 'plai' ),
            'before_widget' => '<div class="sidebar-section">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        ) );

        // Register widgets support.
        register_widget( 'PLAI_Gallery_Widget' );
    }

endif;

add_action( 'widgets_init', 'plai_widgets_init' );


