<?php
/**
 * The theme header.
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500' rel='stylesheet' type='text/css'>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".navbar-fixed-top">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <header  id="section_header" class="navbar-fixed-top main-nav" style="background-image: url(<?php plai_the_theme_root_uri( 'static/img/icons/h2.png' ); ?>);" role="banner">
        <div class="container"  >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>">
                    <div class="logo-bk"></div>
                    <img src="<?php plai_the_theme_root_uri( 'static/img/logo2.png' ); ?>" alt="logo" />
                </a>
            </div><!--Navbar header End-->
            <nav class="collapse navbar-collapse navigation" id="bs-example-navbar-collapse-1" role="navigation">
<?php
                wp_nav_menu(
                    array(
                        'container' => false,
                        'menu_class' => 'nav navbar-nav navbar-right',
                        'theme_location' => 'primary'
                    )
                );
?>
            </nav>
        </div><!-- /.container-fluid -->
    </header>