<?php
/**
 * Custom functions used throughout the theme.
 */



/**
 * Fetch the absolute path to functionality theme files.
 *
 * @param string $path the path to the theme files
 * @return string the absolute path
 */
function plai_get_theme_root( $path = '' ) {
    return PLAI_ROOT_PATH . "/$path";
}



/**
 * Output the absolute path to functionality theme files.
 *
 * @param string $path the path to the theme files
 */
function plai_the_theme_root( $path = '' ) {
    echo plai_get_theme_root( $path );
}



/**
 * Fetch the absolute URI to static assets.
 *
 * @param string $path the absolute path to the static asset
 * @return string the absolute URI
 */
function plai_get_theme_root_uri( $path = '' ) {
    return PLAI_ROOT_URI . "/$path";
}



/**
 * Output the absolute URI to static assets.
 *
 * @param string $path the absolute path to the static asset
 */
function plai_the_theme_root_uri( $path = '' ) {
    echo plai_get_theme_root_uri( $path );
}



/**
 * Enable upscaling images upon thumbnail generation,
 * if original image width/height not at the expected values.
 */
function plai_maybe_upscale_image( $default, $orig_width, $orig_height, $new_width, $new_height, $crop ) {
    // Do nothing if crop disabled.
    if ( ! $crop ) {
        return null;
    }

    // Establish the aspect ratio.
    $size_ratio = max( $new_width / $orig_width, $new_height / $orig_height );

    $crop_width = round( $new_width / $size_ratio );
    $crop_height = round( $new_height / $size_ratio );

    // Establish crop positioning as centered on both vertical and horizontal axes.
    $start_x = floor( ( $orig_width - $crop_width ) / 2 );
    $start_y = floor( ( $orig_height - $crop_height ) / 2 );

    // Handle custom crop positioning, if specified.
    if ( is_array( $crop ) ) {
        //Handles horizontal crop positioning.
        if ( $crop[0] === 'left' ) {
            $start_x = 0;
        } else if ( $crop[0] === 'right' ) {
            $start_x = $orig_width - $crop_width;
        }

        //Handles vertical crop positioning.
        if ( $crop[1] === 'top' ) {
            $start_y = 0;
        } else if ( $crop[1] === 'bottom' ) {
            $start_y = $orig_height - $crop_height;
        }
    }

    return array( 0, 0, (int) $start_x, (int) $start_y, (int) $new_width, (int) $new_height, (int) $crop_width, (int) $crop_height );
}

add_filter( 'image_resize_dimensions', 'plai_maybe_upscale_image', 10, 6 );



/**
 * Fetch the participant category out of the post categories.
 *
 * @return string the participant category slug
 */
function plai_get_participant_category() {
    $participants_categories = get_categories( array( 'parent' => 'participanti', 'fields' => 'names' ) );
    $post_categories = wp_get_post_categories( get_the_ID() );
    foreach ( $post_categories as $category ) {
    	if ( in_array( $category, $participants_categories ) ) {
    		var_dump( $category );
    		return $category;
    	}
    }
    return false;
}



/**
 * Callback function which creates the HTML of
 * the comment elements in the list of comments.
 */
function plai_the_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    extract( $args, EXTR_SKIP );

    $has_children = empty( $args['has_children'] ) ? 'media ' : 'parent media';
?>
    <li <?php comment_class( $has_children ) ?> id="comment-<?php comment_ID() ?>" data-comment-id="<?php comment_ID(); ?>">
        <span class="media-left">
            <?php echo get_avatar( get_comment_author_email(), 95, plai_get_theme_root_uri( 'static/img/defaults/plai-95-95.png' ), false, array( 'class' => 'img-responsive' ) ); ?>
        </span>
        <div class="media-body">
            <h4 class="media-heading"><?php comment_author(); ?><span><i><?php echo strtolower( get_comment_date( 'j F Y' ) ); ?></span></i></h4>
            <?php comment_text(); ?>
<?php
            comment_reply_link(
                array_merge(
                    $args,
                    array(
                        'add_below' => '',
                        'reply_text' => 'Răspunde',
                        'login_text' => 'Conectează-te pentru a lăsa un comentariu',
                        'depth' => $depth,
                        'max_depth' => $args['max_depth']
                    )
                )
            );
?>
        </div>
<?php
}



/**
 * For the schedule page, also load the Roboto Slab font.
 */
function plai_load_font_roboto_slab() {
    if ( is_page( 'program' ) ) {
        echo '<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:700" rel="stylesheet">';
    }
}

add_action( 'wp_head', 'plai_load_font_roboto_slab' );