<?php

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



if ( ! class_exists( 'PLAI_Post_Types' ) ) :

    class PLAI_Post_Types {

        /**
         * Function which retrieves the singleton class instance.
         */
        public static function run_instance() {
            static $instance = null;
            if ( $instance === null ) {
                $instance = new static();
            }

            return $instance;
        }



        /*
         * Empty functions.
         */
        protected function __clone() {}
        protected function __wakeup() {}


        /**
         * The constructor function.
         */
        protected function __construct() {
            add_action( 'init', array( $this, 'add_post_types_support' ) );
        }



        /**
         * Function which registers the extra post types.
         */
        public function add_post_types_support() {
            $labels = array(
                'name'               => esc_html__( 'Participants', 'plai' ),
                'singular_name'      => esc_html__( 'Participant', 'plai' ),
                'menu_name'          => esc_html__( 'Participants', 'plai' ),
                'name_admin_bar'     => esc_html__( 'Participant', 'plai' ),
                'add_new'            => esc_html__( 'Add New', 'plai' ),
                'add_new_item'       => esc_html__( 'Add New Participant', 'plai' ),
                'new_item'           => esc_html__( 'New Participant', 'plai' ),
                'edit_item'          => esc_html__( 'Edit Participant', 'plai' ),
                'view_item'          => esc_html__( 'View Participant', 'plai' ),
                'all_items'          => esc_html__( 'All Participants', 'plai' ),
                'search_items'       => esc_html__( 'Search Participants', 'plai' ),
                'parent_item_colon'  => esc_html__( 'Parent Participants:', 'plai' ),
                'not_found'          => esc_html__( 'No participants found.', 'plai' ),
                'not_found_in_trash' => esc_html__( 'No participants found in Trash.', 'plai' )
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_nav_menus'  => true,
                'show_in_menu'       => true,
                'show_in_admin_bar'  => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'participanti' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'supports'           => array( 'title', 'editor', 'thumbnail' ),
                'taxonomies'         => array( 'category', 'post_tag' )
            );

            // Add the support for the participant post type.
            register_post_type( 'participant', $args );

            $labels = array(
                'name'              => _x( 'Participant Types', 'taxonomy general name', 'plai' ),
                'singular_name'     => _x( 'Participant Type', 'taxonomy singular name', 'plai' ),
                'search_items'      => __( 'Search Participant Types', 'plai' ),
                'all_items'         => __( 'All Participant Types', 'plai' ),
                'parent_item'       => __( 'Parent Participant Type', 'plai' ),
                'parent_item_colon' => __( 'Parent Participant Type:', 'plai' ),
                'edit_item'         => __( 'Edit Participant Type', 'plai' ),
                'update_item'       => __( 'Update Participant Type', 'plai' ),
                'add_new_item'      => __( 'Add New Participant Type', 'plai' ),
                'new_item_name'     => __( 'New Participant Type Name', 'plai' ),
                'menu_name'         => __( 'Participant Type', 'plai' )
            );

            $args = array(
                'public' => false,
                'hierarchical' => true,
                'show_ui'      => true,
                'labels'       => $labels
            );

            register_taxonomy( 'participant-type', 'participant', $args );
//Testimonials
                        $labels = array(
                'name'               => esc_html__( 'Testimonials', 'plai' ),
                'singular_name'      => esc_html__( 'Testimonial', 'plai' ),
                'menu_name'          => esc_html__( 'Testimonials', 'plai' ),
                'name_admin_bar'     => esc_html__( 'Testimonial', 'plai' ),
                'add_new'            => esc_html__( 'Add New', 'plai' ),
                'add_new_item'       => esc_html__( 'Add New Testimonial', 'plai' ),
                'new_item'           => esc_html__( 'New Testimonial', 'plai' ),
                'edit_item'          => esc_html__( 'Edit Testimonial', 'plai' ),
                'view_item'          => esc_html__( 'View Testimonial', 'plai' ),
                'all_items'          => esc_html__( 'All Testimonials', 'plai' ),
                'search_items'       => esc_html__( 'Search Testimonials', 'plai' ),
                'parent_item_colon'  => esc_html__( 'Parent Testimonials:', 'plai' ),
                'not_found'          => esc_html__( 'No testimonials found.', 'plai' ),
                'not_found_in_trash' => esc_html__( 'No testimonials found in Trash.', 'plai' )
            );

            $args = array(
                'labels'             => $labels,
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_nav_menus'  => true,
                'show_in_menu'       => true,
                'show_in_admin_bar'  => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'testimonials' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'supports'           => array( 'title', 'editor', 'thumbnail' ),
                'taxonomies'         => array( 'category', 'post_tag' )
            );

            // Add the support for the participant post type.
            register_post_type( 'testimonial', $args );

            $labels = array(
                'name'              => _x( 'Testimonial Types', 'taxonomy general name', 'plai' ),
                'singular_name'     => _x( 'Testimonial Type', 'taxonomy singular name', 'plai' ),
                'search_items'      => __( 'Search Testimonial Types', 'plai' ),
                'all_items'         => __( 'All Testimonial Types', 'plai' ),
                'parent_item'       => __( 'Parent Testimonial Type', 'plai' ),
                'parent_item_colon' => __( 'Parent Testimonial Type:', 'plai' ),
                'edit_item'         => __( 'Edit Testimonial Type', 'plai' ),
                'update_item'       => __( 'Update Testimonial Type', 'plai' ),
                'add_new_item'      => __( 'Add New Testimonial Type', 'plai' ),
                'new_item_name'     => __( 'New Testimonial Type Name', 'plai' ),
                'menu_name'         => __( 'Testimonial Type', 'plai' )
            );

            $args = array(
                'public' => false,
                'hierarchical' => true,
                'show_ui'      => true,
                'labels'       => $labels
            );

            register_taxonomy( 'testimonial-type', 'testimonial', $args );
//Projects
                        $labels = array(
                'name'               => esc_html__( 'Projects', 'plai' ),
                'singular_name'      => esc_html__( 'Project', 'plai' ),
                'menu_name'          => esc_html__( 'Projects', 'plai' ),
                'name_admin_bar'     => esc_html__( 'Project', 'plai' ),
                'add_new'            => esc_html__( 'Add New', 'plai' ),
                'add_new_item'       => esc_html__( 'Add New Project', 'plai' ),
                'new_item'           => esc_html__( 'New Project', 'plai' ),
                'edit_item'          => esc_html__( 'Edit Project', 'plai' ),
                'view_item'          => esc_html__( 'View Project', 'plai' ),
                'all_items'          => esc_html__( 'All Projects', 'plai' ),
                'search_items'       => esc_html__( 'Search Projects', 'plai' ),
                'parent_item_colon'  => esc_html__( 'Parent Projects:', 'plai' ),
                'not_found'          => esc_html__( 'No Projects found.', 'plai' ),
                'not_found_in_trash' => esc_html__( 'No Projects found in Trash.', 'plai' )
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_nav_menus'  => true,
                'show_in_menu'       => true,
                'show_in_admin_bar'  => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'proiecte' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'supports'           => array( 'title', 'editor', 'thumbnail' ),
                'taxonomies'         => array( 'category', 'post_tag' )
            );

            // Add the support for the project post type.
            register_post_type( 'project', $args );

        $labels = array(
                'name'              => _x( 'Project Types', 'taxonomy general name', 'plai' ),
                'singular_name'     => _x( 'Project Type', 'taxonomy singular name', 'plai' ),
                'search_items'      => __( 'Search Project Types', 'plai' ),
                'all_items'         => __( 'All Project Types', 'plai' ),
                'parent_item'       => __( 'Parent Project Type', 'plai' ),
                'parent_item_colon' => __( 'Parent Project Type:', 'plai' ),
                'edit_item'         => __( 'Edit Project Type', 'plai' ),
                'update_item'       => __( 'Update Project Type', 'plai' ),
                'add_new_item'      => __( 'Add New Project Type', 'plai' ),
                'new_item_name'     => __( 'New Project Type Name', 'plai' ),
                'menu_name'         => __( 'Project Type', 'plai' )
            );

            $args = array(
                'public' => false,
                'hierarchical' => true,
                'show_ui'      => true,
                'labels'       => $labels
            );

            register_taxonomy( 'project-type', 'project', $args );

            //Sponsors
                        $labels = array(
                'name'               => esc_html__( 'Sponsors', 'plai' ),
                'singular_name'      => esc_html__( 'Sponsor', 'plai' ),
                'menu_name'          => esc_html__( 'Sponsors', 'plai' ),
                'name_admin_bar'     => esc_html__( 'Sponsor', 'plai' ),
                'add_new'            => esc_html__( 'Add New', 'plai' ),
                'add_new_item'       => esc_html__( 'Add New Sponsor', 'plai' ),
                'new_item'           => esc_html__( 'New Sponsor', 'plai' ),
                'edit_item'          => esc_html__( 'Edit Sponsor', 'plai' ),
                'view_item'          => esc_html__( 'View Sponsor', 'plai' ),
                'all_items'          => esc_html__( 'All Sponsors', 'plai' ),
                'search_items'       => esc_html__( 'Search Sponsors', 'plai' ),
                'parent_item_colon'  => esc_html__( 'Parent Sponsors:', 'plai' ),
                'not_found'          => esc_html__( 'No Sponsors found.', 'plai' ),
                'not_found_in_trash' => esc_html__( 'No Sponsors found in Trash.', 'plai' )
            );

            $args = array(
                'labels'             => $labels,
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_nav_menus'  => true,
                'show_in_menu'       => true,
                'show_in_admin_bar'  => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'proiecte' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'supports'           => array( 'title', 'editor', 'thumbnail' ),
                'taxonomies'         => array( 'category', 'post_tag' )
            );

            // Add the support for the Sponsor post type.
            register_post_type( 'Sponsor', $args );

        $labels = array(
                'name'              => _x( 'Sponsor Types', 'taxonomy general name', 'plai' ),
                'singular_name'     => _x( 'Sponsor Type', 'taxonomy singular name', 'plai' ),
                'search_items'      => __( 'Search Sponsor Types', 'plai' ),
                'all_items'         => __( 'All Sponsor Types', 'plai' ),
                'parent_item'       => __( 'Parent Sponsor Type', 'plai' ),
                'parent_item_colon' => __( 'Parent Sponsor Type:', 'plai' ),
                'edit_item'         => __( 'Edit Sponsor Type', 'plai' ),
                'update_item'       => __( 'Update Sponsor Type', 'plai' ),
                'add_new_item'      => __( 'Add New Sponsor Type', 'plai' ),
                'new_item_name'     => __( 'New Sponsor Type Name', 'plai' ),
                'menu_name'         => __( 'Sponsor Type', 'plai' )
            );

            $args = array(
                'public' => false,
                'hierarchical' => true,
                'show_ui'      => true,
                'labels'       => $labels
            );

            register_taxonomy( 'Sponsor-type', 'Sponsor', $args );
        }

    }

endif;

PLAI_Post_Types::run_instance();