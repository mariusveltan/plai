<?php
/**
 * Custom template tags used throughout the theme.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



if ( ! function_exists( 'plai_get_search_query' ) ):

    /**
     * Display the search query keyword, if present.
     */
    function plai_get_search_query() {
        if ( isset( $_GET['q'] ) ) {
            return $_GET['q'];
        }
        return '';
    }

endif;



if ( ! function_exists( 'plai_get_listing_featured_image' ) ) :

    /**
     * Fetch the details of the category featured image.
     *
     * @param string $image_size the size of the image to fetch.
     *
     * @return array the url and HTML for the featured image.
     */
    function plai_get_listing_featured_image( $image_size, $css_classes = '', $post_id = 0 ) {
        global $post;

        if ( $post_id === 0 ) {
            $post_id = $post->ID;
        }

        $image_details = array(
                            'url'  => '',
                            'html' => ''
                        );
        if ( has_post_thumbnail( $post_id ) ) {
            $image_id = get_post_thumbnail_id( $post_id );
        } else {
            // Fetch an image from the media uploaded for this post.
            $image = get_children( array(
                                        'post_parent' => $post_id,
                                        'post_type'   => 'attachment',
                                        'numberposts' => '1'
                                    ));
            if ( ! empty( $image ) ) {
                $image = array_pop( $image );
                $image_id = $image->ID;
            }
        }
        // Fetch the image URL.
        if ( ! empty( $image_id ) ) {
            $image = wp_get_attachment_image_src( $image_id, $image_size );
        } else {
            // If no image was found, fetch a default.
            $image = array( plai_get_theme_root_uri( "/static/img/defaults/$image_size.png" ) );   
        }
        if ( is_array( $image ) ) {
            $image_details['url'] = $image[0];
            $image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
            if ( $image_alt == '' ) {
                $image_alt = get_the_title( $post_id );
            }
            $css_classes = ( $css_classes ) ? sprintf( 'class="%s"', $css_classes ) : '';
            $image_details['html'] = sprintf( '<img src="%s" alt="%s" %s />', $image_details['url'], esc_attr( $image_alt ), $css_classes );
        }
        return $image_details;
    }

endif;



if ( ! function_exists( 'plai_the_listing_featured_image' ) ) :

    /**
     * Output the HTML of the category featured image.
     *
     * @param string $image_size the size of the image to fetch.
     *
     * @return array the url and HTML for the featured image.
     */
    function plai_the_listing_featured_image( $image_size, $css_classes = '', $post_id = 0 ) {
        $image_details = plai_get_listing_featured_image( $image_size, $css_classes, $post_id = 0 );
        echo $image_details['html'];
    }

endif;



if ( ! function_exists( 'plai_get_the_category' ) ) :

    /**
     * Get the main category associated to a post.
     * 
     * @param integer $post_id the ID of the post for which to fetch the category
     *
     * @return object the main category for the current post
     */
    function plai_get_the_category( $post_id = 0 ) {
        global $post, $wp_query;
        
        $category = null;

        if ( ! $post_id && $post ) {
            $post_id = $post->ID;
        }

        if ( $post_id ) {
            $categories = get_the_category( $post_id );
            if ( count( $categories ) > 0 ) {
                $category = $categories[0];
            }
        } elseif ( is_category() ) {
            $category = $wp_query->get_queried_object();
        }

        return $category;
    }

endif;



if ( ! function_exists( 'plai_the_listing_category' ) ) :

    /**
     * Output the HTML of the category link on a listing page.
     */
    function plai_the_listing_category() {
        $category = plai_get_the_category();
        $category_name = ( $category ) ? $category->name : '';
        $category_link = ( $category ) ? get_term_link( $category->slug, 'category' ) : '';
        if ( $category ) {
            printf( '<a href="%1$s" class="post-category-link" title="%2$s"><h4>%2$s</h4></a>', $category_link, esc_attr( $category_name ) );
        }
    }

endif;



if ( ! function_exists( 'plai_get_the_excerpt' ) ) :

    /**
     * Get a trimmed version of the excerpt.
     *
     * @param integer $num_chars the max number of characters to return
     * @return string the trimmed excerpt
     */
    function plai_get_the_excerpt( $num_chars ) {
        $excerpt = get_the_excerpt();
        if ( strlen( $excerpt ) > $num_chars ) {
            // Shorten the excerpt.
            $excerpt = substr( $excerpt, 0, $num_chars );
            // Find the position at which the last word begins.
            $pos = strrpos( $excerpt, ' ' );
            // Cut the characters after the last space in excerpt.
            $excerpt = substr( $excerpt, 0, $pos );
            $excerpt .= '&hellip;';
        }

        return $excerpt;
    }

endif;



if ( ! function_exists( 'plai_the_excerpt' ) ) :

    /**
     * Output a trimmed version of the excerpt.
     *
     * @param integer $num_chars the max number of characters to return
     */
    function plai_the_excerpt( $num_chars ) {
        echo plai_get_the_excerpt( $num_chars );
    }

endif;



if ( ! function_exists( 'plai_the_breadcrumb' ) ) :

    /**
     * Create the breadcrumb, according
     * to the page currently displayed.
     */
    function plai_the_breadcrumb() {
        // Fetch the breadcrumb trail generated.
        //$trail = PLAI_Breadcrumbs::get_trail();
        if ( $trail !== false ):
?>
            <div class="post-breadcrumb">
                <ol class="path">
                    <?php foreach ( $trail as $crumb ): ?>
                        <?php echo $crumb; ?>
                    <?php endforeach; ?>
                </ol>
            </div>
<?php
        endif;
    }

endif;



if ( ! function_exists( 'plai_the_related_articles' ) ) :

    /**
     * Create the section containing related posts.
     */
    function plai_the_related_articles() {
        global $post, $related_query;
        $tag_ids = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );
        $args = array(
                    'tag__in'        => $tag_ids,
                    'post__not_in'   => array( $post->ID ),
                    'posts_per_page' => 3,
                    'ignore_sticky_posts' => true
                );
        $related_query = new WP_Query( $args );
        get_template_part( 'tpl-related-articles-blog' );
        wp_reset_query();
    }

endif;



if ( ! function_exists( 'plai_the_suggested_participants' ) ) :

    /**
     * Create the section suggesting other festival participants.
     */
    function plai_the_suggested_participants() {
        global $post, $related_query;
        $tag_ids = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );
        $args = array(
                    'post_type'      => 'participant',
                    'tag__in'        => $tag_ids,
                    'post__not_in'   => array( $post->ID ),
                    'posts_per_page' => 3,
                    'orderby'        => 'rand'
                );
        $related_query = new WP_Query( $args );
        get_template_part( 'tpl-related-articles-participants' );
        wp_reset_query();
    }

endif;



if ( ! function_exists( 'plai_the_related_pages' ) ) :

    /**
     * Create the section containing related pages
     */
    function plai_the_related_pages() {
        global $post, $related_query;
        $tag_ids = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );
        $args = array(
                    'tag__in'        => $tag_ids,
                    'post__not_in'   => array( $post->ID ),
                    'posts_per_page' => 3,
                    'ignore_sticky_posts' => true
                );
        $related_query = new WP_Query( $args );
        get_template_part( 'tpl-related-articles-info' );
        wp_reset_query();
    }

endif;



if ( ! function_exists( 'plai_get_the_participant_type' ) ) :

    /**
     * Fetch the participant types associated to the current participant ID.
     *
     * @param integer $participant_id The ID of the current participant
     * @return string The collection of participant types, separated by blanks
     */
    function plai_get_the_participant_type( $participant_id ) {
        $participant_types = wp_get_post_terms( $participant_id, 'participant-type', array() );
        $participant_type = '';
        foreach ( $participant_types as $type ) {
            $participant_type .= ' ' . $type->slug; 
        }
        return $participant_type;
    }

endif;