<?php

class PLAI_Gallery_Widget extends WP_Widget {

    /**
     * Set up the widget.
     */
    public function __construct() {
        parent::__construct(
            'plai_gallery_widget',
            __( 'PLAI Gallery Widget', 'plai' ),
            array( 'description' => __( 'A widget for displaying the article images within the article sidebar.', 'plai' ), )
        );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        $widget_title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : 'Galerie';
        $images = get_attached_media( 'image' );
        if ( $images ) {
            $output  = $args['before_widget'];
            $output .= '<div class="photo-stream clearfix">';
            $output .= sprintf( '<h4>%s</h4>', $widget_title );
            foreach ( $images as $image ) {
                $image_details = wp_get_attachment_image_src( $image->ID, 'thumbnail' );
                $image_alt = get_post_meta( $image->ID, '_wp_attachment_image_alt', true );
                $image_page_url = get_attachment_link( $image->ID );
                $output .= '<div class="b-stream">';
                $output .= sprintf(
                                '<a href="%s"><img src="%s" alt="%s" class="img-responsive"></a>',
                                esc_url( $image_page_url ),
                                esc_url( $image_details[0] ),
                                $image_alt
                            );
                $output .= '</div>'; // b-stream
            }
            $output .= '</div>'; // photo-stream
            $output .= $args['after_widget'];
        }

        echo $output;
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) { ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Widget Title', 'plai' ); ?></label></p>
        <p><input type='text' class='widefat' id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>"/></p>
    <?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }
}
