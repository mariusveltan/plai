<?php
/**
 * The template for rendering the site landing page.
 */

get_header(); ?>
  
            <section id="banner">
            <div class="container">
                <div class="row">
                    <div class="blog-header text-center">
                        <h2>Din ogradă</h2>
                        <ul class="breadcrumb">
                            <li><?php the_title(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
<section id="contact">
    <div class="container">
        <div class="row">
  			<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Keep In <b>touch</b></h3>
                    <h4 class="feature_sub">Centrul Cultural PLAI este o organizaţie tânără care îşi propune promovarea artei şi a multiculturalităţii, dorind să creeze şi să ofere ocazia de dezvoltare prin diversitate. Lasă-ne un mesaj sau scrie-ne pe adresa noastră de e-mail: birou@plai.ro </h4>
                    <div class="divider"></div>
                </div>
  			</div>
        </div>
        <div class="row">
<?php
            /* Generate contact form via Cotnact Form 7.
               Source for Contact Form 7 form:

               <div class="contact_full clearfix">
                    <div class="col-md-6 left">
                        <div class="left_contact">
                            <form action="role">
                                <div class="form-level">
                                    [text* name id:name class:input-block placeholder "Nume"]
                                    <span class="form-icon fa fa-user"></span>
                                </div>
                                <div class="form-level">
                                    [email* email id:mail class:input-block placeholder "Email"]
                                    <span class="form-icon fa fa-envelope-o"></span>
                                </div>
                                <div class="form-level">
                                    [tel phone id:phone class:input-block placeholder "Telefon"]
                                    <span class="form-icon fa fa-phone"></span>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-6 right">
                        <div class="form-level">
                            [textarea* message id:message class:textarea-block placeholder "Mesajul tău"]
                            <span class="form-icon fa fa-pencil"></span>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        [submit class:btn class:btn-main class:featured "Trimite"]
                    </div>
                </div>
            */
            if ( have_posts() ) {
                the_post();
                the_content();
            }
?>
        </div>
    </div>
</section>
<?php get_footer(); ?>