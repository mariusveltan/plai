<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        
        <section id="banner" style="background-image: url(<?php plai_the_theme_root_uri( 'static/img/banner/banner1.jpg' ); ?>);">
            <div class="container">
                <div class="row">
                    <div class="blog-header text-center">
                        <h2>Info</h2>
                        <ul class="breadcrumb">
                            <li><?php the_title(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-single">
            <div class="container">
                 <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <h1><?php the_title(); ?></h1>
                        <hr /><div class="clearfix"></div>
                        <?php the_content(); ?>
                    </div>
                    <?php plai_the_related_pages(); ?>
                </div>

                <?php get_sidebar(); ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();