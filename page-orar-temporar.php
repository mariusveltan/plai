<?php
/*
 * Template Name: Custom Page Template
 * Description: Page template 
 */

get_header(); ?>
<section id="contact" style="padding-bottom:25px">
    <div class="container">
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Orar</h3>
                    <h4 class="feature_sub">Joi și vineri deschidem porțile muzeului de la ora 17:00 iar sâmbătă și duminică de la ora 12:00. </h4>
                    <div class="divider"></div>
                </div>
            </div>
        </div>

        <section id="blog-single">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="blog-desc">
<h1>Scena World Music</h1>
<h4>Joi</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%">20:00-21:00</td>
<td width="50%"><a href="http://www.plai.ro/2016/joss-stone.html">Joss Stone</a></td>
</tr>
</tbody>
</table>
<h4>Vineri</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%">20:30-21:30</td>
<td width="50%"><a href="http://www.plai.ro/2016/fantome-artist-rezident.html">Fantome - artist rezident</a></td>
</tr>
<tr>
<td width="15%">22:00-23:30</td>
<td width="50%"><a href="http://www.plai.ro/2016/milky-chance.html">Milky Chance</a></td>
</tr>
</tbody>
</table>
<h4>Sâmbătă</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%">19:00-20:00</td>
<td width="50%"><a href="http://www.plai.ro/2016/fantome-artist-rezident.html">Fantome - artist rezident</a></td>
</tr>
<tr>
<td width="15%">20:15-21:30</td>
<td width="50%"><a href="http://www.plai.ro/2016/aziza-brahim.html">Aziza Brahim</a></td>
</tr>
<tr>
<td width="15%">22:00-23:30</td>
<td width="50%"><a href="http://www.plai.ro/2016/bob-geldof.html">Bob Geldof</a></td>
</tr>
</tbody>
</table>
<h4>Duminică</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%">19:00-20:00</td>
<td width="50%"><a href="http://www.plai.ro/2016/fantome-artist-rezident.html">Fantome - artist rezident</a></td>
</tr>
<tr>
<td width="15%">20:15-21:30</td>
<td width="50%"><a href="http://www.plai.ro/2016/marinah-y-chicuelo.html">Marinah y Chicuelo</a></td>
</tr>
<tr>
<td width="15%">22:00-23:30</td>
<td width="50%">Artist (Detalii în curând)</td>
</tr>
</tbody>
</table>
<div class="spatiul20"></div>
<hr />



<h1>Scena Antagon</h1>
<h4>Vineri</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%"></td>
<td width="50%">Detalii în curând</td>
</tr>
</tbody>
</table>
<h4>Sâmbătă</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%"></td>
<td width="50%">Detalii în curând</td>
</tr>
</tbody>
</table>
<h4>Duminică</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%"></td>
<td width="50%">Detalii în curând</td>
</tr>
</tbody>
</table>
<div class="spatiul20"></div>
                    </div>
                </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h1>Scena after party</h1>
<h4>Vineri</h4>
<table>
<tbody>
<tr>
<td width="15%">23:00 – 5:00</td>
<td width="50%"><a href="http://www.plai.ro/2016/sabotage.html">Sabotage</a></td>
</tr>
</tbody>
</table>
<h4>Sâmbătă</h4>
<table>
<tbody>
<tr>
<td width="15%">23:00 – 5:00</td>
<td width="50%"><a href="http://www.plai.ro/2016/freenetik.html">Freenetik</a></td>
</tr>
</tbody>
</table>
<hr />
<h1>Ceau, Casa Filmelor</h1>
<h4>Vineri</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%"></td>
<td width="50%">Detalii în curând</td>
</tr>
</tbody>
</table>
<h4>Sâmbătă</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%"></td>
<td width="50%">Detalii în curând</td>
</tr>
</tbody>
</table>
<h4>Duminică</h4>
<table width="100%">
<tbody>
<tr>
<td width="15%"></td>
<td width="50%">Detalii în curând</td>
</tr>
</tbody>
</table>

                    </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
</div>
</section>
<?php
get_footer();