<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>
        
<section id="about-details"  class="premii">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title">Premii şi aprecieri</h3>
                    <h4 class="feature_sub"><b>Cu ce ne mândrim<b></h4>
                    <div class="divider"></div>
                </div>
            </div>  <!-- Col-md-12 End -->

            <div id="din-ograda" class="owl-carousel owl-theme team-items">
<?php
              $args = array(
                          'category_name' => 'premii',
                          'posts_per_page' => 5,
                          
                      );
              $blog_posts = new WP_Query( $args );
              while ( $blog_posts->have_posts() ) {
                $blog_posts->the_post();
                get_template_part( 'template-parts/content', 'preview-din-ograda' );
              }
?>
            </div>
        </div>
    </div> <!-- Conatiner Team end -->
    <!-- </div> -->
</section>  
<div class="clearfix"></div>

<?php
get_footer(); ?>