<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        
        <section id="banner">
            <div class="container">
                <div class="row">
                    <div class="text-center carousel-caption">
                        <h3 class="feature_title" style="color:white; margin-left:auto; margin-right:auto">Presă</h3>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-single">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <?php the_content(); ?>                   
                    </div>
                </div>

                <?php get_sidebar(); ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();