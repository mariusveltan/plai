<?php
/**
 * The template for displaying all pages.
 */

// Fetch the participants types.
$participant_types = get_terms( array(
                        'taxonomy'   => 'participant-type',
                        'orderby'    => 'term_id',
                        'hide_empty' => false
                    ));

get_header(); ?>

<div id="plai-schedule" class="schedule">
    <div class="schedule-filters">
        <div class="participant-type-filters">
            <div class="participant-type-filters-wrapper">
                <a class="participant-type-filter current" href="#" data-filter="">All</a>
                <?php foreach ( $participant_types as $type ) : ?>
                    <a class="participant-type-filter" href="#" data-filter="<?php echo esc_attr( $type->slug ); ?>"><?php echo $type->name; ?></a>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="schedule-location-window">
            <div class="schedule-location-wrapper">
                <a class="location-filter schedule-location current" data-filter="" href="#">
                    <h2>All</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="scena-mare" href="#">
                    <h2>Scena Mare</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="scena-mica" href="#">
                    <h2>Scena Mica</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="chill" href="#">
                    <h2>Chill</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="aleea-culturala" href="#">
                    <h2>Aleea Culturala</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="casa-filmelor" href="#">
                    <h2>Casa Filmelor</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="camping" href="#">
                    <h2>Camping</h2>
                </a>
                <a class="location-filter schedule-location" data-filter="food-area" href="#">
                    <h2>Food Area</h2>
                </a>
            </div>
        </div>

        <div class="day-filters">
            <a class="day-filter current" href="#" data-filter="8/9">
                <span class="day-filters-week-day">Jo</span>
                <span class="day-filters-date">8/09</span>
            </a>
            <a class="day-filter" href="#" data-filter="9/9">
                <span class="day-filters-week-day">Vi</span>
                <span class="day-filters-date">9/09</span>
            </a>
            <a class="day-filter" href="#" data-filter="10/9">
                <span class="day-filters-week-day">Sa</span>
                <span class="day-filters-date">10/09</span>
            </a>
            <a class="day-filter" href="#" data-filter="11/9">
                <span class="day-filters-week-day">Du</span>
                <span class="day-filters-date">11/09</span>
            </a>
        </div>
    </div>
    <div class="schedule-scroll">
        <a class="scroll-left hidden"></a>
        <a class="scroll-right hidden"></a>
    </div>
    <div class="schedule-grid-wrapper">
        <div class="schedule-grid">
            <div class="schedule-grid-row" data-hour="12"><span>12pm</span></div>
            <div class="schedule-grid-row" data-hour="13"><span>1pm</span></div>
            <div class="schedule-grid-row" data-hour="14"><span>2pm</span></div>
            <div class="schedule-grid-row" data-hour="15"><span>3pm</span></div>
            <div class="schedule-grid-row" data-hour="16"><span>4pm</span></div>
            <div class="schedule-grid-row" data-hour="17"><span>5pm</span></div>
            <div class="schedule-grid-row" data-hour="18"><span>6pm</span></div>
            <div class="schedule-grid-row" data-hour="19"><span>7pm</span></div>
            <div class="schedule-grid-row" data-hour="20"><span>8pm</span></div>
            <div class="schedule-grid-row" data-hour="21"><span>9pm</span></div>
            <div class="schedule-grid-row" data-hour="22"><span>10pm</span></div>
            <div class="schedule-grid-row" data-hour="23"><span>11pm</span></div>
            <div class="schedule-grid-row" data-hour="0"><span>12am</span></div>
            <div class="schedule-grid-row" data-hour="1"><span>1am</span></div>
            <div class="schedule-grid-row" data-hour="2"><span>2am</span></div>
            <div class="schedule-grid-row" data-hour="3"><span>3am</span></div>
            <div class="schedule-grid-row" data-hour="4"><span>4am</span></div>
            <div class="schedule-grid-row" data-hour="5"><span>5am</span></div>
            <div class="schedule-grid-row" data-hour="6"><span>6am</span></div>
        </div>
        <div class="schedule-events-container">
            <div class="schedule-events">
<?php
                $args = array(
                            'post_type'      => 'participant',
                            'posts_per_page' => -1,
                            'meta_query'     => array( array(
                                                    'key' => '_plai_schedule_details'
                                                ))
                        );
                $participants = new WP_Query( $args );
?>
                <?php while ( $participants->have_posts() ) : ?>
<?php
                    $participants->the_post();
                    $schedule_details = get_post_meta( get_the_ID(), '_plai_schedule_details', true );
                    $participant_type = esc_attr( plai_get_the_participant_type( get_the_ID() ) );
                    
                    foreach ( $schedule_details['timetable'] as $event ) :
                        $day = date( 'j/n', strtotime( str_replace('/', '-', $event['day'] ) ) );
                        $start_time = date( 'g:i A', strtotime( $event['start_hour'] . ':' . $event['start_minute'] ) );
                        $end_time = date( 'g:i A', strtotime( $event['end_hour'] . ':' . $event['end_minute'] ) );
?>
                        <a class="schedule-event hidden" data-type="<?php echo $participant_type; ?>" data-location="<?php echo $schedule_details['location']; ?>" data-date="<?php echo esc_attr( $day ); ?>" data-start-time="<?php echo esc_attr( $event['start_hour'] . ':' . $event['start_minute'] ); ?>" data-end-time="<?php echo esc_attr( $event['end_hour'] . ':' . $event['end_minute'] ); ?>" style="top: 10px; left:40px" href="<?php the_permalink(); ?>">
                            <div class="event-description-wrapper">
                                <div class="event-description">
                                    <h3 class="event-title"><?php echo $event['activity']; ?></h3>
                                    <span class="event-author"><?php the_title(); ?></span>
                                    <span class="event-time"><?php echo $start_time . ' - ' . $end_time; ?></span>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();