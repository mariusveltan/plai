<?php
/**
 * The template for displaying participants.
 */

// Fetch the testimonials.
$args = array(
    'post_type' => 'testimonial',
    'posts_per_page' => -1,
);
$query = new WP_Query( $args );

// Fetch the participants types.
/*$testimonial_types = get_terms( array(
                'taxonomy'   => 'testimonial-type',
                'orderby'    => 'term_id',
                'hide_empty' => false
            ));
*/
$testimonial_types = get_terms('testimonial-type');
get_header();
?> 

<section id="portfolio" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="feature_header text-center">
                    <h3 class="feature_title"><b>#sieusuntPLAI</b></h3>
                   <!-- <h4 class="feature_sub"></h4> -->
                    <div class="divider"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="isotope-filter" class="skew3 text-center clearfix">
        <a data-filter="*" href="#" class="active">All</a>
        <?php foreach ( $testimonial_types as $type ) : ?>
            <a href="#" class="" data-filter=".<?php echo esc_attr( $type->slug ); ?>"><?php echo $type->name; ?></a>
        <?php endforeach; ?>
    </div>
    <div class="text-center">

        <ul class="portfolio-wrap" id="portfolio_items" style="font-color:white">
            <?php while($query->have_posts()) : ?>
                <?php $query->the_post(); ?>
                <?php get_template_part('template-parts/content', 'preview-testimonial'); ?>
            <?php endwhile; ?>
        </ul>
    </div>
</section>
<?php get_footer(); ?>