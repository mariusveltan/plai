<?php

/**

 * The template for displaying participants.

 */



// Fetch the participants.

$args = array(

    'post_type' => 'sponsor',

     'posts_per_page' => -1,

);

$query = new WP_Query( $args );



// Fetch the participants types.

$sponsor_types = get_terms( array(

                'taxonomy'   => 'sponsor-type',

                'orderby'    => 'term_id',

                'hide_empty' => false

            ));



get_header();

?> 



<section id="portfolio" class="clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="feature_header text-center">

                    <h3 class="feature_title"> Susţin PLAI<b></b></h3>

                    <h4 class="feature_sub"> </h4>

                    <div class="divider"></div>

                </div>

            </div>

        </div>

    </div>



    <div id="isotope-filter" class="skew3 text-center clearfix">

        <a data-filter="*" href="#" class="active">All</a>

        <?php foreach ( $sponsor_types as $type ) : ?>

            <a href="#" class="" data-filter=".<?php echo esc_attr( $type->slug ); ?>"><?php echo $type->name; ?></a>

        <?php endforeach; ?>

    </div>

    <div class="text-center">



        <ul class="portfolio-wrap" id="portfolio_items">

            <?php while($query->have_posts()) : ?>

                <?php $query->the_post(); ?>

                <?php get_template_part('template-parts/content', 'preview-sponsor'); ?>

            <?php endwhile; ?>

        </ul>

    </div>

</section>

<?php get_footer(); ?>