<?php
/*
 * Template Name: Custom Page Template
 * Description: Page template 
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        
        <section id="banner" style="background-image: url(<?php plai_the_theme_root_uri( 'static/img/banner/banner1.jpg' ); ?>);">
            <div class="container">
                <div class="row">


                    <div class="blog-header text-center">
                    <div class="carousel-caption">
                        <div class="col-md-12 col-xs-12 text-center">
                      <h3 class="feature_title" style="color:white; margin-left:auto; margin-right:auto"><?php the_title(); ?></h3> 
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-single">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();