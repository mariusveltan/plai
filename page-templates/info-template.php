<?php
/*
 * Template Name: Info Pages Template
 * Description: Page template 
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        
        <section id="banner" style="background-image: url(<?php plai_the_theme_root_uri( 'static/img/banner/blog-banner.jpg' ); ?>);">
            <div class="container">
                <div class="row">


                    <div class="blog-header text-center">
                    <div class="carousel-caption" style="    margin-top: 50px;">
                        <div class="col-md-12 col-xs-12 text-center">
                      <h3 class="feature_title" style="color:white; margin-left:auto; margin-right:auto"><?php the_title(); ?></h3> 
                      <div class="line" style="color:green;"></div>
                      <h4 class="feature_sub" style="font-size:25px"><b>Informaţii utile<b></h4>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-single">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <h1><?php the_title(); ?></h1>
                        <hr />
                        <?php the_content(); ?>
                    </div>
                    <?php plai_the_related_pages(); ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();