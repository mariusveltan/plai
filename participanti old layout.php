<?php
/**
 * The template for displaying all single participants.
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
<?php
        $banner_image = '';
        if ( has_post_thumbnail() ) {
            $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
            $banner_image = sprintf( 'style="background-image:url(\'%s\')"', esc_url( $image[0] ) );
        }
?>   
        <div id="banner" <?php echo $banner_image; ?>>
            <div class="container">
                <div class="row">
                    <div class="blog-header text-center">
                            <h2><?php the_title(); ?></h2>
                    </div>
                </div>
            </div>

        </div>

        <section id="blog-single">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <h4><?php the_title(); ?></h4>
                        <ul class="post-meta-links list-inline">
                            <li><a href="#"><span> <i class="fa fa-bookmark"></i></span> <?php echo strtolower( get_the_time( 'j F Y' ) ); ?></a></li>
                        </ul>
                        <?php the_content(); ?>
                    </div>
                    <hr />

                    <div class="clearfix"></div>

                    <?php plai_the_suggested_participants(); ?>
                </div>

                <?php get_sidebar( 'participant' ); ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();