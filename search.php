<?php
/**
 * The template for displaying search results pages.
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <div class="page-search">
        <h1 class="search-title">Rezultate căutării pentru: <?php echo get_search_query(); ?></h1>
        <div class="search-results">
            <?php while ( have_posts() ) : ?>
                <?php the_post(); ?>
                <?php get_template_part( 'template-parts/content', 'preview-din-ograda' ); ?>
            <?php endwhile; ?>
        </div>
    </div>
<?php else : ?>
    <?php get_template_part( 'template-parts/content', 'none' ); ?>
<?php endif; ?>

<?php
get_footer();