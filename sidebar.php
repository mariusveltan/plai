<?php
/**
 * The sidebar containing the main widget area.
 */
?>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="blog-sidebar">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <div class="blog-search">
                    <div class="serach">
                        <input name="s" placeholder="Caută" type="search" value="<?php echo get_search_query(); ?>" />
                        <button><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>

            <?php if ( is_active_sidebar( 'article-sidebar' ) ) : ?>
                <?php dynamic_sidebar( 'article-sidebar' ); ?>
            <?php endif; ?>

            <div class="share">
<?php
            $article_title = urlencode( get_the_title() );
            $article_url = urlencode( get_permalink() );
            if ( has_post_thumbnail() ) {
                $article_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
                $article_image = urlencode( $article_image[0] );
            } else {
                $article_image = '';
            }
?>
            <ul class="social-contact list-inline">
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $article_url; ?>" onclick="return plai_pop_new_window('https://www.facebook.com/sharer/sharer.php?u=<?php echo $article_url; ?>')"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/intent/tweet?url=<?php echo $article_url; ?>" onclick="return plai_pop_new_window('https://twitter.com/intent/tweet?url=<?php echo $article_url; ?>&amp;text=<?php echo $article_title; ?>&amp;via=PLAIFestival')"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://plus.google.com/share?url=<?php echo $article_url; ?>" onclick="return plai_pop_new_window('https://plus.google.com/share?url=<?php echo $article_url; ?>')"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="https://pinterest.com/pin/create/link/?url=<?php echo $article_url; ?>&amp;media=<?php echo $article_image; ?>&amp;description=<?php echo $article_title; ?>" onclick="return plai_pop_new_window('https://pinterest.com/pin/create/link/?url=<?php echo $article_url; ?>&amp;media=<?php echo $article_image; ?>&amp;description=<?php echo $article_title; ?>')"><i class="fa fa-pinterest"></i></a></li>
            </ul>
        </div>
    </div>
</div>