<?php

/**

 * The template for displaying all single participants.

 */



get_header(); ?>



<?php

        $banner_image = '';

        if ( has_post_thumbnail() ) {

            $image = wp_get_attachment_image_src( get_post_thumbnail_id());

            $banner_image = sprintf( 'style="background-image:url(\'%s\')"', esc_url( $image[0]));

        }

?> 

<section id="participant-single">

    <div class="container" >

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="feature_header text-center" >



                    <h3 class="feature_title"><?php the_title(); ?></h3>

                    <h4 class="feature_sub"> 

                        <ul class="post-meta-links list-inline">

                            <li><span> <i class="fa fa-bookmark"></i></span> <?php echo strtolower( get_the_time( 'j F h:i' ) ); ?></li>

                        </ul>

                    </h4>

                    <div class="divider"></div>
                    <div>

            <img src="<?php echo esc_url($image[0]); ?>" />

            </div>
                </div>

            </div>

        </div>

        <div class="row">

<?php

            if ( have_posts() ) {

                the_post();

                the_content();

            }

?> 

                   <hr />

                    <div class="clearfix"></div>

                    <?php plai_the_suggested_participants(); ?>

        </div>

    </div>

</section>

<?php get_footer(); ?>

