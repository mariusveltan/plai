<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        
        <section id="banner">
            <div class="container">
                <div class="row">
                    <div class="blog-header text-center">
                        <h2>Din ogradă</h2>
                        <ul class="breadcrumb">
                            <li><?php the_title(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-single" class="blog-post">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-desc">
                        <h4><?php the_title(); ?></h4>
                        <ul class="post-meta-links list-inline">
                            <li><a href="#"><span> <i class="fa fa-bookmark"></i></span> <?php echo strtolower( get_the_time( 'j F Y' ) ); ?></a></li>
                            <li><a href="#"> <span><i class="fa fa-comments"></i></span><?php comments_number( '0', '1', '%' ); ?></a></li>
                        </ul>
                        <?php the_content(); ?>
                    </div>
                    <hr />

                    <div class="clearfix"></div>

                    <?php plai_the_related_articles(); ?>

                    <?php if ( comments_open() || get_comments_number() ) : ?>
                        <?php comments_template(); ?>
                    <?php endif; ?>
                </div>

                <?php get_sidebar(); ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();