( function( $ ) {

    'use strict';
    
    /**
     * Global function for opening pop-up window.
     *
     * @param string url - the URL to use for the new window
     */
    window.plai_pop_new_window = function( url ) {
        var new_window = window.open( url, 'name',' height=400,width=600' );
        if ( window.focus ) {
            new_window.focus();
        }
        return false;
    }



    /**
     * Adjust the height of the content area on all pages.
     */
    function adjust_content_min_height() {
        var $content_element = $( '#blog-single' );
        $content_element.css({ 'min-height' : '' });
        var body_height = $( 'body' ).height();
        var window_height = $( window ).height();
        if ( body_height < window_height ) {
            var footer_height = $( '#footer' ).height();
            var content_height = window_height - footer_height;
            $content_element.css({ 'min-height' : content_height + 'px' });
        }
    }



    var PLAI_Schedule = function() {
        var self = this;
        var _$schedule = $( '#plai-schedule' );
        // Set the grid row height and width;
        var _grid_row_height = 40;
        var _grid_row_width  = 140;
        // Declar var for the number of grid rows.
        var _grid_num_rows;
        /* Set the top and left padding, to add to event elements
         * to align them to the first grid row, first grid column. */
        var _default_top_offset  = 10;
        var _default_left_offset = 45;
        // Declare matrix for keeping track of occupied grid positions.
        var _positioning_matrix;



        /**
         * Compute the height of an event element, according to the
         * duration of the event. Also determine the top offset,
         * according to the start hour of the event.
         */
        var _set_height_and_vertical_position_of_event_elements = function() {
            $( '.schedule-event', _$schedule ).each( function() {
                var $event = $( this );
                var start_time = $event.data( 'start-time' );
                start_time = start_time.split( ':' ).map( function( num ) { return parseInt( num ); } );
                var end_time = $event.data( 'end-time' );
                end_time = end_time.split( ':' ).map( function( num ) { return parseInt( num ); } );
                var num_grid_rows;
                if ( start_time[0] > end_time[0] ) {
                    // Event extends towards the following day.
                    num_grid_rows = 24 - start_time[0] + end_time[0];
                } else {
                    // Event end in the same day.
                    num_grid_rows = end_time[0] - start_time[0];
                }
                // Multiply by 2, since one row is 30 mins.
                num_grid_rows *= 2;
                // Also take into consideration the ending minutes.
                if ( end_time[1] > 30 ) {
                    num_grid_rows += 2;
                } else if ( end_time[1] > 0 ) {
                    num_grid_rows += 1;
                }
                // Set the height of the event element.
                $event.outerHeight( num_grid_rows * _grid_row_height );

                // Now compute the top offset, taking into account the minutes, as well.
                var num_grid_rows_top = ( start_time[0] - parseInt( $( '.schedule-grid-row', _$schedule ).first().data( 'hour' ) ) ) * 2;
                if ( start_time[1] > 30 ) {
                    num_grid_rows_top += 1;
                }
                $event.css({ top : ( _default_top_offset + num_grid_rows_top * _grid_row_height ) + 'px' });
                // Also store info as data attributes.
                $event.data( 'row-start', num_grid_rows_top );
                $event.data( 'row-end', num_grid_rows_top + num_grid_rows );
            });
        };



        /**
         * Position the event elements on page init and on changing
         * the active filters. Filter values are passed as params.
         * If empty, no filtering is applied for the respective filter categories.
         *
         * @param participant_type The participant type selected for filtering
         * @param location The location selected for filtering
         * @param day The day selected for filtering
         */
        var _position_active_event_elements = function( participant_type, location, day ) {
            /* We'll use a bi-dimensional matrix for identifying rows and cols that
             * are occupied by the active schedule events elements, to avoid overlapping. */
            _positioning_matrix = [];
            for ( var row_index = 0; row_index < _grid_num_rows; row_index ++ ) {
                _positioning_matrix.push( [] );
            }

            // Hide all event elements, for repositioning.
            $( '.schedule-event', _$schedule ).removeClass( 'active' ).addClass( 'hidden' ).css({ left: '' });
            
            $( '.schedule-event:not(.active)', _$schedule ).each( function() {
                var $event = $( this );

                if ( ( participant_type != '' ) && ( $event.data( 'type' ).indexOf( participant_type ) == -1 ) ) {
                    return;
                }
                if ( ( location != '' ) && ( $event.data( 'location' ).indexOf( location ) == -1 ) ) {
                    return;
                }
                if ( $event.data( 'date' ) != day ) {
                    return;
                }

                var col_index = 0;
                var ok_to_place = false;
                // Determine the column where it is ok to place the current event element.
                while ( ok_to_place == false ) {
                    ok_to_place = true;
                    for ( var row_index = $event.data( 'row-start' ); row_index <= $event.data( 'row-end' ); row_index ++ ) {
                        if ( ( typeof _positioning_matrix[ row_index ][ col_index ] !== 'undefined' ) &&
                                ( _positioning_matrix[ row_index ][ col_index ] == 1 ) ) {
                            ok_to_place = false;
                            col_index ++;
                            break;
                        }
                    }
                }
                // Set the positioning matrix, to indicate occupied positions.
                for ( var row_index = $event.data( 'row-start' ); row_index <= $event.data( 'row-end' ); row_index ++ ) {
                    _positioning_matrix[ row_index ][ col_index ] = 1;
                }
                // Set the element left offset and make it visible.
                $event.css({ left : ( _default_left_offset + col_index * _grid_row_width ) + 'px' }).addClass( 'active' ).removeClass( 'hidden' );
            });

            var grid_container_width = 0;
            $( '.schedule-event.active', _$schedule ).each( function() {
                grid_container_width += $( this ).outerWidth( true );
            });
            $( '.schedule-events', _$schedule ).width( grid_container_width );

            // Trigger event at end of repositioning elements.
            $( window ).trigger( 'plai_elements_repositioned' );
        };



        /**
         * Initialize the schedule functionality.
         */
        self.init = function() {
            // Set the number of grid rows.
            _grid_num_rows = $( '.schedule-grid-row', _$schedule ).length * 2;

            // Set the event elements height and top offset.
            _set_height_and_vertical_position_of_event_elements();

            // Determine the date to use at initialization.
            var init_date = '8/9';
            var today = new Date();
            var today_day = parseInt( today.getUTCDate() );
            var today_month = parseInt( today.getUTCMonth() ) + 1;
            if ( ( today_month == 9 ) && ( today_day >= 8 ) && ( today_day <= 11 ) ) {
                init_date = today_day + '/' + today_month;
            }
            // Initialize the position of the event elements.
            _position_active_event_elements( '', '', init_date );

            // Add event handler for switching the participant type filters.
            _$schedule.on( 'click', '.participant-type-filter', function( event ) {
                event.preventDefault();
                $( '.participant-type-filter', _$schedule ).removeClass( 'current' );
                $( this ).addClass( 'current' );
                // Reposition event elements.
                _position_active_event_elements(
                    $( '.participant-type-filter.current', _$schedule ).data( 'filter' ),
                    $( '.location-filter.current', _$schedule ).data( 'filter' ),
                    $( '.day-filter.current', _$schedule ).data( 'filter' )
                );
            });
            
            // Add event handler for switching the location filters.
            _$schedule.on( 'click', '.location-filter', function( event ) {
                event.preventDefault();
                $( '.location-filter', _$schedule ).removeClass( 'current' );
                $( this ).addClass( 'current' );
                // Reposition event elements.
                _position_active_event_elements(
                    $( '.participant-type-filter.current', _$schedule ).data( 'filter' ),
                    $( '.location-filter.current', _$schedule ).data( 'filter' ),
                    $( '.day-filter.current', _$schedule ).data( 'filter' )
                );
            });
            
            // Add event handler for switching the day filters.
            _$schedule.on( 'click', '.day-filter', function( event ) {
                event.preventDefault();
                $( '.day-filter', _$schedule ).removeClass( 'current' );
                $( this ).addClass( 'current' );
                // Reposition event elements.
                _position_active_event_elements(
                    $( '.participant-type-filter.current', _$schedule ).data( 'filter' ),
                    $( '.location-filter.current', _$schedule ).data( 'filter' ),
                    $( '.day-filter.current', _$schedule ).data( 'filter' )
                );
            });

            // Add handler for making the schedule grid draggable.
            var draggable = false;
            $( window ).on( 'load resize plai_elements_repositioned', function() {
                if ( $( '.schedule-events', _$schedule ).width() > $( window ).width() ) {
                    $( '.schedule-events', _$schedule ).draggable({ 
                        cursor: 'move',
                        containment: '.schedule-events-container',
                        stop: function() {
                            $( '.schedule-events', _$schedule ).css( 'top', '0' );
                        }
                    });
                    draggable = true;
                } else {
                    draggable = false;
                }
            });
        };
    };



    $( document ).ready( function() {
        // Adjust the content min height for the pages.
        if ( $( '#blog-single' ).length == 1 ) {
            adjust_content_min_height();
            $( window ).resize( function() {
                adjust_content_min_height();
            });
        }

        // Enable the schedule functionality.
        if ( $( '#plai-schedule' ).length == 1 ) {
            ( new PLAI_Schedule() ).init();
        }
    });

})( jQuery );