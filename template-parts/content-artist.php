<?php
$participant_category = plai_get_participant_category();
?>
<?php if ( ! $participant_category ) : ?>
	<li class="col-xs-12 col-sm-6 col-md-3 single-portfolio <?php echo $participant_category; ?>" style="">
	    <figure>
	        <img src="images/parteneriCulturali/p5.jpg" alt="" />
	        <figcaption>
	            <h5><?php the_title(); ?></h5>
	            <p class="links">
	                <a href="portfolio-single.html"> <i class="fa fa-link"></i></a>
	                <a href="images/parteneriCulturali/p5.jpg" data-rel="prettyPhoto" class="img-responsive">
	                    <i class="fa fa-plus"></i>
	                </a>
	            </p>
	            <p class="description">
	               Află mai multe
	            </p>
	        </figcaption>
	    </figure>
	</li>
<?php endif; ?>