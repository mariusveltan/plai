<?php
/**
 * The template for displaying 404 pages (not found).
 */

get_header(); ?>

<section id="blog-single" class="page-404">
    <div class="container">
        <h2 class="page-404-subtitle">Nu există conținut de afișat pentru această pagină...</h2>
        <a class="page-404-link" href="<?php echo home_url( '/' ); ?>" title="Înapoi acasă">&raquo; Înapoi acasă</a>
    </div>
</section>

<?php
get_footer();