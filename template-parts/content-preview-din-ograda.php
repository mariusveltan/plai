<div class="item text-center">
    <div class="single-member">
        <div class="overlay-hover">
            <a class="overlay-hover-link" href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
                <?php plai_the_listing_featured_image( 'post-thumbnail', 'img-responsive' ); ?>
            </a>
            <?php $excerpt = plai_get_the_excerpt( 180 ); ?>
            <?php if ( strlen( $excerpt ) > 0 ) : ?>
                <div class="overlay-effect">
                    <p><?php echo $excerpt; ?></p>
                </div>
            <?php endif; ?>
        </div> 
        <div class="post-date"><span><?php the_time( 'j' ); ?></span><?php the_time( 'n' ); ?></div>
        <h3><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_title(); ?></a></h3>
        <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><h5> citește mai departe &raquo;</h5></a>
    </div>
</div>