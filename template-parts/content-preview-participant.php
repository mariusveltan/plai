<li class="col-xs-12 col-sm-6 col-md-3 single-portfolio participant-class <?php echo esc_attr( plai_get_the_participant_type( get_the_ID() ) ); ?>">
    <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
        <figure>
            <?php plai_the_listing_featured_image( 'plai-500-403' ); ?>
            <figcaption>
                <h5><?php the_title(); ?></h5>
                <p class="description">
                   Află mai multe
                </p>
            </figcaption>
        </figure>
    </a>
</li>