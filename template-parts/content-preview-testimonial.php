
<?php
$testimonial_types = wp_get_post_terms( get_the_ID(), 'testimonial-type', array() );
$testimonial_type = '';
foreach ( $testimonial_types as $type ) {
    $testimonial_type .= ' ' . $type->slug; 
}
?>

<li class="col-xs-12 col-sm-6 col-md-3 single-portfolio style="width: 365px; height: 365px;" <?php echo esc_attr( $testimonial_type ); ?>">
    <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
        <figure style="width:365px">
            <?php plai_the_listing_featured_image( 'plai-500-403' ); ?>
            <figcaption style="height:100%">
                <u><h5><?php the_title(); ?></h5></u>
                <p class="description" style="color:white">
                   <?php the_content(); ?>
                </p>
            </figcaption>
        </figure>
    </a>
</li>