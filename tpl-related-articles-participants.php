<?php
/**
 * Template for displaying the related posts. Template
 * is called both by YARPP plugin, if active, as well as
 * by the theme functionality for fetching related posts.
 */

global $related_query;
?>

<?php if ( $related_query->have_posts() ): ?>
    <div class="related-post ">
        <br />
        <h4>Printre participanți</h4>
        <br />
        <?php while ( $related_query->have_posts() ) : ?>
            <?php $related_query->the_post(); ?>
            <div class="col-md-4 col-sm-4">
                <div class="rel-post">
                    <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
                        <?php plai_the_listing_featured_image( 'plai-225-140', 'img-responsive' ); ?>
                        <div class="caption">
                            <h4><?php the_title(); ?></h4>
                            <p><?php plai_the_excerpt( 65 ); ?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>